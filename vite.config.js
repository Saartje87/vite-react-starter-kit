import preact from '@preact/preset-vite';
import { vanillaExtractPlugin } from '@vanilla-extract/vite-plugin';
import { defineConfig } from 'vite';
import { VitePWA } from 'vite-plugin-pwa';

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    https: false,
  },
  plugins: [
    preact(),
    vanillaExtractPlugin(),
    VitePWA({
      // registerType: 'autoUpdate',
      manifest: {
        name: 'Starter Kit',
        short_name: 'S-Kit',
      },
      workbox: {
        globPatterns: ['**/*.{js,css,html,svg}'],
      },
    }),
  ],
  resolve: {
    alias: {
      react: 'preact/compat',
      'react-dom': 'preact/compat',
    },
  },
  // Example for working with linked packages
  // optimizeDeps: {
  //   link: ['@playtwo/core'],
  //   exclude: ['@playtwo/core'],
  // },
});
