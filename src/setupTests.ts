export interface Global {
  CSS: { supports: (query: string) => boolean };
}

declare let global: Global;

global.CSS = {
  supports() {
    return true;
  },
};
