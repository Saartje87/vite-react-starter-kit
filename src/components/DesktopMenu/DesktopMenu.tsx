import { FC, useLayoutEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { useBreakpoint } from '../../hooks/useBreakpoint';
import { Atoms } from '../../sprinkles.css';
import { isDrawerOpen } from '../../store/drawer/drawerSelectors';
import { Box } from '../Box/Box';
import { Stack } from '../Stack/Stack';
import { desktopBackgroundMenuButtonClass, desktopMenuClass } from './DesktopMenu.css';

type Props = Required<Pick<Atoms, 'color'>>;

export const DesktopMenu: FC<Props> = ({ color, children }) => {
  const drawerOpen = useSelector(isDrawerOpen);
  const containerRef = useRef<HTMLDivElement>(null);
  const backgroundRef = useRef<HTMLDivElement>(null);
  const [counter, setCounter] = useState(0);

  // Trigger layoutEffect when breakpoint matches to make sure
  // the background is placed on correct position
  useBreakpoint('tablet', (matches) => {
    if (matches) {
      setCounter((count) => count + 1);
    }
  });

  useLayoutEffect(() => {
    if (!containerRef.current || !backgroundRef.current) {
      return;
    }

    const activeButton = containerRef.current.querySelector('[aria-current="true"]');

    if (!activeButton) {
      return;
    }

    const rect = activeButton.getBoundingClientRect();
    const scrollTop = containerRef.current.scrollTop;

    backgroundRef.current.style.transform = `translateY(${rect.top + scrollTop - 15}px)`;
  }, [color, drawerOpen, counter]);

  return (
    <Box
      ref={containerRef}
      display={['none', 'block']}
      background={color}
      className={desktopMenuClass}
      overflow="auto"
    >
      <Stack spacing="large" alignItems="center">
        {children}
      </Stack>

      <Box ref={backgroundRef} background="white" className={desktopBackgroundMenuButtonClass} />
    </Box>
  );
};
