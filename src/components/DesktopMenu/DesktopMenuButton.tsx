import { Route, useHistory } from '@blockle/router';
import React, { FC } from 'react';
import { Atoms } from '../../sprinkles.css';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import { Icon, IconNames } from '../Icon/Icon';
import { desktopMenuButtonClass, desktopMenuButtonInactiveClass } from './DesktopMenu.css';

interface Props extends Required<Pick<Atoms, 'color'>> {
  label: string;
  to: string;
  icon: IconNames;
}

export const DesktopMenuButton: FC<Props> = ({ label, color, to, icon }) => {
  const { push } = useHistory();

  function onClick(event: React.SyntheticEvent<HTMLAnchorElement, MouseEvent>) {
    event.preventDefault();
    push(to);
  }

  return (
    <Route
      path={to}
      render={(match) => {
        return (
          <Box
            as="a"
            href={to}
            title={label}
            aria-label={label}
            aria-current={match}
            display="flex"
            placeItems="center"
            background={color}
            color={color}
            className={cx(desktopMenuButtonClass, !match && desktopMenuButtonInactiveClass)}
            onClick={onClick}
          >
            <Icon name={icon} color="white" />
          </Box>
        );
      }}
    />
  );
};
