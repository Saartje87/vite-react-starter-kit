import { style } from '@vanilla-extract/css';
import { theme } from '../../theme.css';

export const desktopMenuClass = style({
  position: 'relative',
  zIndex: 0,
  width: 50,
  minWidth: 50,
  paddingTop: 90,
  paddingBottom: 25,
  scrollbarWidth: 'none',
  selectors: {
    '&::-webkit-scrollbar': {
      display: 'none',
    },
  },
});

export const desktopMenuButtonClass = style({
  position: 'relative',
  appearance: 'none',
  border: 'none',
  width: 36,
  height: 36,
  borderRadius: '100%',
  cursor: 'pointer',
  outline: 0,
  transition: 'filter 200ms',
  ':focus': {
    boxShadow: `0 0 0 1px ${theme.color.white}, 0 0 0 3px currentColor`,
  },
  ':focus-visible': {
    boxShadow: `0 0 0 1px ${theme.color.white}, 0 0 0 3px currentColor`,
  },
  selectors: {
    '&:focus:not(:focus-visible)': {
      boxShadow: 'none',
    },
  },
});

export const desktopMenuButtonInactiveClass = style({
  filter: 'brightness(90%);',
});

export const desktopBackgroundMenuButtonClass = style({
  position: 'absolute',
  zIndex: -1,
  height: 46,
  width: 48,
  right: 0,
  top: 0,
  borderRadius: '100% 0 0 100%',
  transition: 'transform 200ms cubic-bezier(0.33, 1, 0.68, 1)',
  '::before': {
    content: '""',
    position: 'absolute',
    right: 0,
    top: -19.6, // Decimal value fixes glitch with background on high dpi screens
    width: 20,
    height: 20,
    background: theme.color.white,
    clipPath:
      'polygon(0% 0%, 100% 0%, 100% 100%, 97% 77%, 92% 57%, 82% 36%, 69% 22%, 48% 9%, 26% 3%)',
    transform: 'rotate(90deg)',
  },
  '::after': {
    content: '""',
    position: 'absolute',
    right: 0,
    bottom: -19.6, // Decimal value fixes glitch with background on high dpi screens
    width: 20,
    height: 20,
    background: theme.color.white,
    clipPath:
      'polygon(0% 0%, 100% 0%, 100% 100%, 97% 77%, 92% 57%, 82% 36%, 69% 22%, 48% 9%, 26% 3%)',
  },
});
