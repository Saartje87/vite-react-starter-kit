import { FC } from 'react';
import { Atoms } from '../../sprinkles.css';
import { Box } from '../Box/Box';
import { Logo } from '../Logo/Logo';
import { panelHeaderClass } from './PanelHeader.css';

type Props = Required<Pick<Atoms, 'color'>>;

export const PanelHeader: FC<Props> = ({ color }) => {
  return (
    <Box paddingLeft={{ tablet: 'gutter' }} flexGrow={1}>
      <Box
        display="flex"
        padding="gutter"
        alignItems="center"
        background={color}
        className={panelHeaderClass}
      >
        <Logo />
      </Box>
    </Box>
  );
};
