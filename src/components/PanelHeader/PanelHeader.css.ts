import { style } from '@vanilla-extract/css';

export const panelHeaderClass = style({
  height: 45,
  borderRadius: '0 0 0 6px',
});
