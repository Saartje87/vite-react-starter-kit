import { forwardRef, HTMLAttributes, ReactNode } from 'react';
import { Atoms } from '../../sprinkles.css';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import { textClass } from './Text.css';

export interface Props
  extends Pick<
      Atoms,
      'textAlign' | 'fontSize' | 'fontWeight' | 'color' | 'fontStyle' | 'textDecoration'
    >,
    Omit<HTMLAttributes<HTMLSpanElement>, keyof Atoms> {
  children: ReactNode;
  as?: 'span' | 'p' | 'strong' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
  className?: string;
}

export const Text = forwardRef<HTMLSpanElement, Props>(function Text(
  {
    as = 'span',
    children,
    className,
    color = 'darkBlue',
    fontSize = 'medium',
    fontStyle = 'normal',
    fontWeight = 'regular',
    textAlign = 'left',
    ...restProps
  }: Props,
  ref,
) {
  return (
    <Box
      ref={ref}
      as={as}
      className={cx(textClass, className)}
      textAlign={textAlign}
      fontSize={fontSize}
      fontWeight={fontWeight}
      color={color}
      fontStyle={fontStyle}
      {...restProps}
    >
      {children}
    </Box>
  );
});
