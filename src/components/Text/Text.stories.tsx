// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { theme } from '../../theme.css';
import { Props as TextProps, Text } from './Text';

export default {
  title: 'Text',
  component: Text,
  argTypes: {
    children: {
      type: 'string',
    },
    className: {
      control: false,
    },
    fontSize: {
      type: 'select',
      options: Object.keys(theme.fontSize),
    },
  },
} as Meta;

const Template: Story<TextProps> = (args) => <Text {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: 'Text text',
  as: 'span',
  color: 'darkBlue',
  fontSize: 'medium',
  fontStyle: 'normal',
  fontWeight: 'regular',
  textAlign: 'left',
  textDecoration: 'none',
};
