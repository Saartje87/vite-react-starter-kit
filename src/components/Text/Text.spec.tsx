import '@testing-library/jest-dom/extend-expect';
import { render } from '../../utils/testUtils';
import { Text } from './Text';

describe('Text', () => {
  it('should render children', () => {
    const { getByText } = render(<Text color="blue">Text content</Text>);

    expect(getByText('Text content')).toBeTruthy();
  });
});
