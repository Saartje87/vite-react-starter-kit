import { FC } from 'react';
import logoUrl from '../../assets/logo.svg';

export const Logo: FC = () => {
  return <img src={logoUrl} alt="Logo" />;
};
