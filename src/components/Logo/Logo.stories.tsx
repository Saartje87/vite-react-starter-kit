// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { Box } from '../Box/Box';
import { Logo } from './Logo';

export default {
  title: 'Logo',
  component: Logo,
} as Meta;

const Template: Story = (args) => (
  <Box background="grey" padding="medium">
    <Logo {...args} />
  </Box>
);

export const Default = Template.bind({});
Default.args = {};
