import { useHistory } from '@blockle/router';
import { FC, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useBreakpoint } from '../../hooks/useBreakpoint';
import { closeDrawer } from '../../store/drawer/drawerActions';
import { getDrawerColor, isDrawerOpen } from '../../store/drawer/drawerSelectors';
import { Box } from '../Box/Box';
import { Button } from '../Button/Button';
import { DesktopMenu } from '../DesktopMenu/DesktopMenu';
import { DesktopMenuButton } from '../DesktopMenu/DesktopMenuButton';
import { MobileMenuButton } from '../MobileMenuButton/MobileMenuButton';
import { Panel } from '../Panel/Panel';
import { PanelHeader } from '../PanelHeader/PanelHeader';
import { Stack } from '../Stack/Stack';
import { drawerItems } from './drawerItems';

export const Drawer: FC = ({ children }) => {
  const panelRef = useRef<HTMLDivElement>(null);
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const dispatch = useDispatch();
  const drawerOpen = useSelector(isDrawerOpen);
  const color = useSelector(getDrawerColor);
  const { push } = useHistory();

  // Close mobile menu on > tablet breakpoint
  useBreakpoint('tablet', (matches) => {
    if (matches) {
      setShowMobileMenu(false);
    }
  });

  useEffect(() => {
    function onClick(event: MouseEvent) {
      if (!(event.target instanceof Element)) {
        return;
      }

      const parent = event.target.parentNode;

      if (parent instanceof Element && parent.id === 'root') {
        dispatch(closeDrawer());
      }
    }

    document.addEventListener('click', onClick);

    return () => {
      document.removeEventListener('click', onClick);
    };
  }, []);

  return (
    <Panel ref={panelRef} display="flex" alignItems="stretch" open={drawerOpen}>
      <DesktopMenu color={color}>
        {drawerItems.map(({ name, icon, to }) => (
          <DesktopMenuButton key={name} color={color} label={name} icon={icon} to={to} />
        ))}
      </DesktopMenu>

      <Box flexGrow={1} display="flex" flexDirection="column">
        <Box display="flex">
          <MobileMenuButton onClick={() => setShowMobileMenu((shown) => !shown)} />
          <PanelHeader color={color} />
        </Box>
        <Box
          overflow="hidden"
          position="relative"
          display="flex"
          flexDirection="column"
          flexGrow={1}
        >
          {children}
        </Box>

        <Box
          position="absolute"
          background="white"
          padding="gutter"
          style={{ top: 45, left: 0, width: '100%', height: '100%' }}
          display={showMobileMenu ? 'block' : 'none'}
          aria-hidden={!showMobileMenu}
        >
          <Stack spacing="small">
            {drawerItems.map(({ name, to }) => (
              <Button
                key={name}
                onClick={() => {
                  push(to);
                  setShowMobileMenu(false);
                }}
              >
                {name}
              </Button>
            ))}
          </Stack>
        </Box>
      </Box>
    </Panel>
  );
};
