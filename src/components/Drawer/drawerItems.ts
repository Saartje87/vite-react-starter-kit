import { IconNames } from '../Icon/Icon';

interface DrawerItem {
  name: string;
  icon: IconNames;
  to: string;
}

export const drawerItems: DrawerItem[] = [
  {
    name: 'Interactions',
    icon: 'interaction',
    to: '/interactions',
  },
  {
    name: 'Products',
    icon: 'shopping-cart',
    to: '/products',
  },
  {
    name: 'Settings',
    icon: 'settings',
    to: '/settings',
  },
];
