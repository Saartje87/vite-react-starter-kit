import { forwardRef, HTMLAttributes, ReactNode } from 'react';
import { Atoms } from '../../sprinkles.css';
import { Text } from '../Text/Text';

export interface Props
  extends Pick<Atoms, 'textAlign' | 'fontSize' | 'fontWeight' | 'color' | 'fontStyle'>,
    Omit<HTMLAttributes<HTMLHeadingElement>, keyof Atoms> {
  as?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
  children: ReactNode;
  className?: string;
}

export const Heading = forwardRef<HTMLHeadingElement, Props>(function Heading(
  {
    as = 'h1',
    children,
    className,
    color = 'darkBlue',
    fontSize = 'large',
    fontStyle = 'normal',
    fontWeight = 'bold',
    textAlign = 'left',
    ...restProps
  }: Props,
  ref,
) {
  return (
    <Text
      ref={ref}
      as={as}
      className={className}
      textAlign={textAlign}
      fontSize={fontSize}
      fontWeight={fontWeight}
      color={color}
      fontStyle={fontStyle}
      {...restProps}
    >
      {children}
    </Text>
  );
});
