// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { theme } from '../../theme.css';
import { Heading, Props as HeadingProps } from './Heading';

export default {
  title: 'Heading',
  component: Heading,
  argTypes: {
    children: {
      type: 'string',
    },
    className: {
      control: false,
    },
    fontSize: {
      type: 'select',
      options: Object.keys(theme.fontSize),
    },
  },
} as Meta;

const Template: Story<HeadingProps> = (args) => <Heading {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: 'Heading text',
  color: 'darkBlue',
  fontSize: 'large',
  fontStyle: 'normal',
  fontWeight: 'bold',
  textAlign: 'left',
};
