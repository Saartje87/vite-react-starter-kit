import '@testing-library/jest-dom/extend-expect';
import { render } from '../../utils/testUtils';
import { Heading } from './Heading';

describe('Text', () => {
  it('should render children', () => {
    const { getByText } = render(<Heading color="blue">Heading content</Heading>);

    expect(getByText('Heading content')).toBeTruthy();
  });
});
