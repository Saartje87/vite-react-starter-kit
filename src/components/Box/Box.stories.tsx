// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { atomicStyles } from '../../sprinkles.css';
import { mapAtomicStylesToArgTypes } from '../../utils/storybook';
import { Box, Props as BoxProps } from './Box';

export default {
  title: 'Box',
  component: Box,
  argTypes: {
    children: {
      type: 'string',
    },
    className: {
      control: false,
    },
    as: {
      control: false,
    },
    ...mapAtomicStylesToArgTypes(atomicStyles),
  },
} as Meta;

const Template: Story<BoxProps<'div'>> = (args) => <Box {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: 'Box Text',
  padding: 'small',
};
