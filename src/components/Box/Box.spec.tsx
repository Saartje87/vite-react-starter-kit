import '@testing-library/jest-dom/extend-expect';
import { render } from '../../utils/testUtils';
import { Box } from './Box';

describe('Box', () => {
  it('should render children', () => {
    const { getByText } = render(<Box>Box content</Box>);

    expect(getByText('Box content')).toBeTruthy();
  });

  it('should render atoms', () => {
    const { getByText } = render(<Box padding="gutter">Box content</Box>);

    expect(getByText('Box content').className.match('paddingTop')).toBeTruthy();
  });
});
