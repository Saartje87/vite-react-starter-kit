import { style } from '@vanilla-extract/css';
import { theme } from '../../theme.css';

export const mobileMenuButtonClass = style({
  appearance: 'none',
  height: 45,
  width: 45,
  background: 'transparent',
  border: 'none',
  cursor: 'pointer',
  borderRadius: 6,
  ':active': {
    background: theme.color.grey,
  },
});
