import { FC } from 'react';
import { Box } from '../Box/Box';
import { Icon } from '../Icon/Icon';
import { mobileMenuButtonClass } from './MobileMenuButton.css';

interface Props {
  onClick: () => void;
}

export const MobileMenuButton: FC<Props> = ({ onClick }) => {
  return (
    <Box
      as="button"
      display={['flex', 'none']}
      placeItems="center"
      className={mobileMenuButtonClass}
      onClick={onClick}
    >
      <Icon name="menu" color="blue" />
    </Box>
  );
};
