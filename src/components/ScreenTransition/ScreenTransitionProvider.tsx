import { createContext, FC, useState } from 'react';

export const ScreenTransitionContext = createContext<{
  activeLevel: number;
  activateLevel: (level: number) => () => void;
}>({
  activeLevel: -1,
  activateLevel: () => () => {
    void 0;
  },
});

export const ScreenTransitionProvider: FC = ({ children }) => {
  const [level, setLevel] = useState(-1);

  return (
    <ScreenTransitionContext.Provider
      value={{
        activeLevel: level,
        activateLevel: (level) => {
          setLevel(level);
          return () => {
            setLevel((level) => level - 1);
          };
        },
      }}
    >
      {children}
    </ScreenTransitionContext.Provider>
  );
};
