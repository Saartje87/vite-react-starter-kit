import { FC, useContext, useEffect } from 'react';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import {
  screenTransitionClass,
  screenTransitionEnterClass,
  screenTransitionInactiveClass,
} from './ScreenTransition.css';
import { ScreenTransitionContext } from './ScreenTransitionProvider';

interface Props {
  level: number;
}

export const ScreenTransition: FC<Props> = ({ children, level }) => {
  const context = useContext(ScreenTransitionContext);
  const { activeLevel } = context;

  useEffect(() => {
    return context.activateLevel(level);
  }, []);

  return (
    <Box
      position="absolute"
      display="flex"
      flexDirection="column"
      background="white"
      className={cx(
        screenTransitionClass,
        level > 0 && screenTransitionEnterClass,
        level < activeLevel && screenTransitionInactiveClass,
      )}
    >
      {children}
    </Box>
  );
};
