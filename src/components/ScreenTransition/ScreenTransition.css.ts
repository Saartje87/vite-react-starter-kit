import { keyframes, style } from '@vanilla-extract/css';

const screenTransitionEnter = keyframes(
  {
    '0%': { transform: 'translateX(100%)' },
    '100%': { transform: 'translateX(0%)' },
  },
  'screenTransitionEnter',
);

export const screenTransitionClass = style({
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  transition: 'transform 220ms',
});

export const screenTransitionEnterClass = style({
  animation: `200ms ${screenTransitionEnter}`,
  animationFillMode: 'backwards',
});

export const screenTransitionInactiveClass = style({
  transform: 'translateX(-20%) scaleX(0.95)',
});
