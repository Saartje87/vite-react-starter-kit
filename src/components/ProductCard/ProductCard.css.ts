import { globalStyle, style } from '@vanilla-extract/css';

export const productCardClass = style({
  cursor: 'pointer',
});

export const productCardContentClass = style({
  borderRadius: 6,
});

export const productCardImageWrapperClass = style({
  width: 95,
});

globalStyle(`${productCardImageWrapperClass} > img`, {
  maxHeight: 'calc(100% - 2px)',
  maxWidth: 'calc(100% - 2px)',
  aspectRatio: '4 / 3',
});
