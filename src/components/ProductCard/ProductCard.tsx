import { useHistory } from '@blockle/router';
import { Box } from '../Box/Box';
import { Card } from '../Card/Card';
import { Stack } from '../Stack/Stack';
import { Text } from '../Text/Text';
import {
  productCardClass,
  productCardContentClass,
  productCardImageWrapperClass,
} from './ProductCard.css';

export const ProductCard = () => {
  const { push } = useHistory();

  return (
    <Card
      display="flex"
      background="grey"
      className={productCardClass}
      role="button"
      onClick={() => push('/products/detail/12')}
    >
      <Box display="flex" placeItems="center" className={productCardImageWrapperClass}>
        <img
          src="https://135dip1kp5pb1hxer93f2f2i-wpengine.netdna-ssl.com/wp-content/uploads/2017/12/jcpenney-ugly-sweater-for-two.jpg"
          alt="Product"
        />
      </Box>
      <Box flexGrow={1} padding="gutter" background="white" className={productCardContentClass}>
        <Stack spacing="small">
          <Text fontSize="medium" fontWeight="bold" color="purple">
            Washable middle gauge knit crew neck jumper
          </Text>

          <Stack spacing="small" horizontal alignItems="center">
            <Text fontSize="medium" fontWeight="bold" color="blue">
              $14,95
            </Text>
            <Text fontSize="small" textDecoration="line-through">
              $39,95
            </Text>
          </Stack>
        </Stack>
      </Box>
    </Card>
  );
};
