import { FC, useState } from 'react';
import { registerSW } from 'virtual:pwa-register';
import { Button } from '../Button/Button';
import { Dialog } from '../Dialog/Dialog';
import { Heading } from '../Heading/Heading';
import { Stack } from '../Stack/Stack';

export const ServiceWorker: FC = () => {
  const [updateReady, setUpdateReady] = useState(false);

  const updateSW = registerSW({
    onNeedRefresh() {
      // show a prompt to user
      setUpdateReady(true);
    },
    onOfflineReady() {
      // show a ready to work offline to user
      console.log('Offline ready');
    },
  });

  if (updateReady) {
    return (
      <Dialog open={updateReady}>
        <Stack spacing="large">
          <Heading as="h2">Update found</Heading>
          <Button onClick={() => updateSW()}>Update!</Button>
        </Stack>
      </Dialog>
    );
  }

  return null;
};
