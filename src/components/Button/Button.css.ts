import { style, styleVariants } from '@vanilla-extract/css';
import { breakpoints, theme } from '../../theme.css';

export const buttonBaseClass = style({
  appearance: 'none',
  border: 'none',
  borderRadius: 4,
  cursor: 'pointer',
  outline: 0,
  transition: 'transform 60ms cubic-bezier(.08,.45,.62,1.77)',
  ':active': {
    transform: 'scale(0.96)',
    transitionDuration: '80ms',
  },
  ':focus': {
    boxShadow: `0 0 0 1px ${theme.color.white}, 0 0 0 2px ${theme.color.blue}`,
  },
  ':focus-visible': {
    boxShadow: `0 0 0 1px ${theme.color.white}, 0 0 0 2px ${theme.color.blue}`,
  },
});

export const buttonVariants = styleVariants({
  normal: {
    padding: '13px 12px',
    background: theme.color.blue,
    color: theme.color.white,
    ':disabled': {
      background: theme.color.blueDisabled,
      color: theme.color.grey,
    },
    selectors: {
      '&:hover:not(:disabled)': {
        background: theme.color.blueHover,
      },
      '&:focus:not(:focus-visible)': {
        boxShadow: 'none',
      },
    },
    '@media': {
      [breakpoints.tablet]: {
        padding: '8px 12px',
      },
    },
  },
  outline: {
    padding: '12px 11px',
    background: theme.color.white,
    color: theme.color.blue,
    border: `1px solid ${theme.color.blue}`,
    ':disabled': {
      borderColor: theme.color.blueDisabled,
      color: theme.color.grey,
    },
    selectors: {
      '&:hover:not(:disabled)': {
        background: theme.color.grey,
      },
      '&:focus:not(:focus-visible)': {
        boxShadow: 'none',
      },
    },
    '@media': {
      [breakpoints.tablet]: {
        padding: '7px 11px',
      },
    },
  },
});
