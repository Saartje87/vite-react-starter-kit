// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { Button, Props as ButtonProps } from './Button';
import { buttonVariants } from './Button.css';

export default {
  title: 'Button',
  component: Button,
  argTypes: {
    variant: {
      options: Object.keys(buttonVariants),
    },
    children: {
      type: 'string',
    },
    className: {
      control: false,
    },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Normal = Template.bind({});
Normal.args = {
  variant: 'normal',
  children: 'Button Text',
};

export const Outline = Template.bind({});
Outline.args = {
  variant: 'outline',
  children: 'Button Text',
};
