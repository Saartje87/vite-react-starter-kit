import { ButtonHTMLAttributes, FC, ReactNode } from 'react';
import { Atoms } from '../../sprinkles.css';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import { buttonBaseClass, buttonVariants } from './Button.css';

export interface Props extends Omit<ButtonHTMLAttributes<HTMLButtonElement>, keyof Atoms> {
  children: ReactNode;
  className?: string;
  variant?: keyof typeof buttonVariants;
}

export const Button: FC<Props> = ({ children, className, variant = 'normal', ...restProps }) => {
  return (
    <Box
      as="button"
      type="button"
      fontSize="small"
      fontWeight="bold"
      className={cx(buttonBaseClass, buttonVariants[variant], className)}
      {...restProps}
    >
      {children}
    </Box>
  );
};
