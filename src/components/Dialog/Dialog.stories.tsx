// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { useEffect, useState } from 'react';
import { Box } from '../Box/Box';
import { Button } from '../Button/Button';
import { Dialog, Props as DialogProps } from './Dialog';

export default {
  title: 'Dialog',
  component: Dialog,
  argTypes: {
    children: {
      type: 'string',
    },
    open: {
      type: 'boolean',
    },
    onRequestClose: {
      action: 'RequestClose',
    },
  },
} as Meta;

const Template: Story<DialogProps> = (args) => <Dialog {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: 'Dialog Text',
  open: true,
};

const TemplateTrigger: Story<DialogProps> = (args) => {
  const [open, setOpen] = useState(args.open);

  useEffect(() => {
    setOpen(args.open);
  }, [args.open]);

  return (
    <>
      <Button onClick={() => setOpen(true)}>Open dialog</Button>
      <Dialog open={open} onRequestClose={() => setOpen(false)}>
        <Box>{args.children}</Box>
        <Box display="flex" justifyContent="flex-end">
          <Button onClick={() => setOpen(false)}>Close</Button>
        </Box>
      </Dialog>
    </>
  );
};

export const WithTrigger = TemplateTrigger.bind({});
WithTrigger.args = {
  children: 'Dialog Text',
  open: false,
};
