import { FC, ReactNode, useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';
import { useAnimationState } from '../../hooks/useAnimationState';
import { useFocusTrap } from '../../hooks/useFocusTrap';
import { useLayer } from '../../hooks/useLayer';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import {
  backdropLeave,
  dialogBackdropClass,
  dialogBackdropLeaveClass,
  dialogClass,
  dialogLeaveClass,
  dialogWrapperClass,
} from './Dialog.css';
export interface Props {
  children: ReactNode;
  onRequestClose?: () => void;
  open?: boolean;
}

export const Dialog: FC<Props> = ({ children, open = false, onRequestClose }) => {
  const dialogRef = useRef<HTMLDivElement>(null);
  const focusTrap = useFocusTrap();
  const [state, close] = useAnimationState(open);
  const layer = useLayer();

  useEffect(() => {
    if (!state.open) {
      return;
    }

    const onKeyPress = (event: KeyboardEvent) => {
      if (event.key === 'Escape' && onRequestClose) {
        onRequestClose();
      }
    };

    document.addEventListener('keydown', onKeyPress);

    return () => {
      document.removeEventListener('keydown', onKeyPress);
    };
  }, [state.open]);

  useEffect(() => {
    if (state.open) {
      if (dialogRef.current) {
        focusTrap.enable(dialogRef.current);
      }
    } else {
      focusTrap.disable();
    }
  }, [state.open]);

  const onAnimationEnd = (event: React.AnimationEvent<HTMLDivElement>) => {
    if (event.animationName === backdropLeave) {
      close();
    }
  };

  if (!state.open) {
    return null;
  }

  const dialog = (
    <Box
      ref={dialogRef}
      display="flex"
      placeItems="center"
      className={dialogWrapperClass}
      aria-modal="true"
      role="dialog"
    >
      <div
        className={cx(dialogBackdropClass, state.leave && dialogBackdropLeaveClass)}
        onAnimationEnd={onAnimationEnd}
        onClick={onRequestClose}
      />

      <Box background="white" className={cx(dialogClass, state.leave && dialogLeaveClass)}>
        <Box padding="gutter">{children}</Box>
      </Box>
    </Box>
  );

  return createPortal(dialog, layer());
};
