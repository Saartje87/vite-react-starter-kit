import { keyframes, style } from '@vanilla-extract/css';

const backdropEnter = keyframes({
  '0%': { opacity: 0 },
  '100%': { opacity: 1 },
});

export const backdropLeave = keyframes({
  '0%': { opacity: 1 },
  '100%': { opacity: 0 },
});

export const dialogWrapperClass = style({
  position: 'fixed',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  overflow: 'hidden',
  zIndex: 200,
  contain: 'layout',
});

export const dialogBackdropClass = style({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  background: 'rgba(0, 0, 0, 0.5)',
  animation: `200ms ${backdropEnter}`,
  animationFillMode: 'both',
});

export const dialogBackdropLeaveClass = style({
  animationName: backdropLeave,
});

const dialogEnter = keyframes({
  '0%': { opacity: 0, transform: 'translateY(-20%)' },
  '100%': { opacity: 1, transform: 'translateY(0%)' },
});

const dialogLeave = keyframes({
  '0%': { opacity: 1, transform: 'translateY(0%)' },
  '100%': { opacity: 0, transform: 'translateY(-20%)' },
});

export const dialogClass = style({
  width: 300,
  maxWidth: '90vw',
  maxHeight: '90vh',
  borderRadius: 6,
  animation: `200ms ${dialogEnter}`,
  animationFillMode: 'both',
});

export const dialogLeaveClass = style({
  animationName: dialogLeave,
});
