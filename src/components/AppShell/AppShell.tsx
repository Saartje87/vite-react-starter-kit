import { Router } from '@blockle/router';
import { createHashHistory } from 'history';
import { FC } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { configureStore } from '../../configureStore';
import { ScreenTransitionProvider } from '../ScreenTransition/ScreenTransitionProvider';

type Props = ReturnType<typeof configureStore>;

export const AppShell: FC<Props> = ({ children, store, persistor }) => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ScreenTransitionProvider>
          {/* Use "createMemoryHistory" to disable hash based navigation and thus browser history */}
          <Router history={createHashHistory()}>{children}</Router>
        </ScreenTransitionProvider>
      </PersistGate>
    </Provider>
  );
};
