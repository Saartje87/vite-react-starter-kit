import { style } from '@vanilla-extract/css';
import { theme } from '../../theme.css';

export const extensionButtonClass = style({
  position: 'absolute',
  top: theme.space.gutter,
  left: theme.space.gutter,
  overflow: 'hidden',
  appearance: 'none',
  border: 'none',
  cursor: 'pointer',
  boxShadow: '0px 2px 2px rgba(0, 0, 0, 0.25)',
  borderRadius: '6px',
  width: '60px',
  height: '45px',
  outline: 0,
  transition: 'opacity 220ms, transform 220ms cubic-bezier(0.33, 1, 0.68, 1), visibility 220ms',
  ':hover': {
    background: theme.color.grey,
  },
  ':focus': {
    boxShadow: `0 0 0 1px ${theme.color.white}, 0 0 0 3px ${theme.color.blue}`,
  },
  ':focus-visible': {
    boxShadow: `0 0 0 1px ${theme.color.white}, 0 0 0 3px ${theme.color.blue}`,
  },
  selectors: {
    '&:focus:not(:focus-visible)': {
      boxShadow: '0px 2px 2px rgba(0, 0, 0, 0.25)',
    },
  },
});

export const extensionButtonInactiveClass = style({
  opacity: 0,
  transform: 'translateX(-100%)',
  visibility: 'hidden',
});

export const extensionButtonBarClass = style({
  transition: 'background-color 80ms',
});
