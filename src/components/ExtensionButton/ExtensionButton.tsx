import { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { openDrawer } from '../../store/drawer/drawerActions';
import { isDrawerOpen } from '../../store/drawer/drawerSelectors';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import { Icon } from '../Icon/Icon';
import { Text } from '../Text/Text';
import {
  extensionButtonBarClass,
  extensionButtonClass,
  extensionButtonInactiveClass,
} from './ExtensionButton.css';

export const ExtensionButton: FC = () => {
  const dispatch = useDispatch();
  const drawerOpen = useSelector(isDrawerOpen);

  return (
    <Box
      as="button"
      aria-label="Toggle extension"
      background="white"
      display="flex"
      alignItems="stretch"
      className={cx(extensionButtonClass, drawerOpen && extensionButtonInactiveClass)}
      onClick={() => dispatch(openDrawer())}
    >
      <Box flexGrow={1} display="flex" placeItems="center">
        <Icon name="logo-small" color="darkBlue" />
      </Box>
      <Box
        background={drawerOpen ? 'red' : 'blue'}
        paddingX="small"
        display="flex"
        alignItems="center"
        className={extensionButtonBarClass}
      >
        <Text color="white">{drawerOpen ? '<' : '>'}</Text>
      </Box>
    </Box>
  );
};
