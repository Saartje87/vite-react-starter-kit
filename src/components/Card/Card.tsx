import { ComponentProps, ElementType, forwardRef, ReactElement } from 'react';
import { Atoms } from '../../sprinkles.css';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import { cardClass } from './Card.css';

type OwnProps<T extends ElementType = ElementType> = {
  className?: string;
  as?: T;
} & Atoms;

export type Props<T extends ElementType> = OwnProps<T> & Omit<ComponentProps<T>, keyof OwnProps>;

const defaultElement = 'div';

export const Card: <T extends ElementType = typeof defaultElement>(
  props: Props<T>,
) => ReactElement | null = forwardRef<HTMLDivElement, OwnProps>(function Card(
  { as = 'div', className, ...restProps },
  ref,
) {
  return <Box ref={ref} as={as} className={cx(cardClass, className)} {...restProps} />;
});
