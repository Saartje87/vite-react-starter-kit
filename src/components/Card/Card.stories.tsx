// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { atomicStyles } from '../../sprinkles.css';
import { mapAtomicStylesToArgTypes } from '../../utils/storybook';
import { Card, Props as CardProps } from './Card';

export default {
  title: 'Card',
  component: Card,
  argTypes: {
    children: {
      type: 'string',
    },
    className: {
      control: false,
    },
    as: {
      control: false,
    },
    ...mapAtomicStylesToArgTypes(atomicStyles),
  },
} as Meta;

const Template: Story<CardProps<'div'>> = (args) => <Card {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: 'Card Text',
  padding: 'small',
  background: 'white',
};
