import { style } from '@vanilla-extract/css';

export const cardClass = style({
  borderRadius: 6,
  overflow: 'hidden',
  boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
});
