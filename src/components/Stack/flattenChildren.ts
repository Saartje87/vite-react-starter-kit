import { Children, Fragment, isValidElement, ReactChild, ReactNode } from 'react';

/**
 * Flatten ReactNode
 *
 * @param children ReactNode
 * @param childrenArray ReactChild[]
 * @returns ReactChild[]
 */
export function flattenChildren(
  children: ReactNode,
  childrenArray: ReactChild[] = [],
): ReactChild[] {
  const items = Children.toArray(children);

  items.forEach((item) => {
    if (isValidElement(item)) {
      if (item.type === Fragment) {
        flattenChildren(item.props.children, childrenArray);
      } else {
        childrenArray.push(item);
      }
    } else if (typeof item === 'string' || typeof item === 'number') {
      childrenArray.push(item);
    }
  });

  return childrenArray;
}
