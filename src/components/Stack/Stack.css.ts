import { calc } from '@vanilla-extract/css-utils';
import { createAtomicStyles, createAtomsFn } from '@vanilla-extract/sprinkles';
import { responsiveConfig } from '../../sprinkles.css';
import { theme } from '../../theme.css';

function mapThemeProperty<Data extends Record<string, string>>(
  data: Data,
  mapData: <Key extends keyof Data>(value: Data[Key], key: Key) => string,
): Record<keyof Data, string> {
  const classMap: Record<keyof Data, string> = { ...data };

  for (const key in data) {
    classMap[key] = mapData(data[key], key);
  }

  return classMap;
}

// Map space theme to nagative values. `calc(var(--var) * -1)`
const negativeSpace = mapThemeProperty(theme.space, (value) => calc.multiply(value, -1));

const stackResponsiveStyles = createAtomicStyles({
  ...responsiveConfig,
  properties: {
    marginTop: negativeSpace,
    marginLeft: negativeSpace,
    gap: theme.space,
  },
  shorthands: {
    negativeMarginTop: ['marginTop'],
    negativeMarginLeft: ['marginLeft'],
  },
});

export const stackAtoms = createAtomsFn(stackResponsiveStyles);
