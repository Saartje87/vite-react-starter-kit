// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { responsiveStyles } from '../../sprinkles.css';
import { theme } from '../../theme.css';
import { Box } from '../Box/Box';
import { Props as StackProps, Stack } from './Stack';

export default {
  title: 'Stack',
  component: Stack,
  argTypes: {
    spacing: {
      options: Object.keys(theme.space),
    },
    alignItems: {
      options: Object.keys(responsiveStyles.styles.alignItems.values),
    },
    justifyContent: {
      options: Object.keys(responsiveStyles.styles.justifyContent.values),
    },
    horizontal: {
      control: 'boolean',
    },
    className: {
      control: false,
    },
    as: {
      control: false,
    },
  },
} as Meta;

const Template: Story<StackProps> = (args) => (
  <Stack {...args}>
    <Box background="blue" padding="small">
      A
    </Box>
    <Box background="blue" padding="small">
      B
    </Box>
    <Box background="blue" padding="small">
      C
    </Box>
    <Box background="blue" padding="small">
      D
    </Box>
    <Box background="blue" padding="small">
      E
    </Box>
    <Box background="blue" padding="small">
      F
    </Box>
  </Stack>
);

export const Normal = Template.bind({});
Normal.args = {
  spacing: 'small',
};
