import { forwardRef, HTMLAttributes, ReactNode } from 'react';
import { Atoms } from '../../sprinkles.css';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import { flattenChildren } from './flattenChildren';
import { stackAtoms } from './Stack.css';

const browserSupportsGap = CSS.supports('gap: 1rem');

type StackAtoms = Parameters<typeof stackAtoms>[0];

export interface Props extends Omit<HTMLAttributes<HTMLDivElement>, keyof Atoms> {
  alignItems?: Atoms['alignItems'];
  children: ReactNode;
  className?: string;
  as?: 'div' | 'ol' | 'ul';
  horizontal?: boolean;
  justifyContent?: Atoms['justifyContent'];
  spacing: StackAtoms['marginTop'];
}

export const Stack = forwardRef<HTMLDivElement, Props>(function Stack(
  {
    alignItems = 'flex-start',
    children,
    className,
    as = 'div',
    horizontal,
    justifyContent = 'flex-start',
    spacing,
    ...restProps
  }: Props,
  ref,
) {
  if (browserSupportsGap) {
    return (
      <Box
        ref={ref}
        as={as}
        display="flex"
        flexDirection={horizontal ? 'row' : 'column'}
        justifyContent={justifyContent}
        alignItems={alignItems}
        className={cx(
          stackAtoms({
            gap: spacing,
          }),
          className,
        )}
        {...restProps}
      >
        {children}
      </Box>
    );
  }

  const items = flattenChildren(children);

  return (
    <Box
      ref={ref}
      as={as}
      display="flex"
      flexDirection={horizontal ? 'row' : 'column'}
      justifyContent={justifyContent}
      alignItems={alignItems}
      className={cx(
        stackAtoms({
          negativeMarginTop: horizontal ? undefined : spacing,
          negativeMarginLeft: horizontal ? spacing : undefined,
        }),
        className,
      )}
      {...restProps}
    >
      {items.map((child, key) => (
        <Box
          key={key}
          paddingTop={horizontal ? undefined : spacing}
          paddingLeft={horizontal ? spacing : undefined}
        >
          {child}
        </Box>
      ))}
    </Box>
  );
});
