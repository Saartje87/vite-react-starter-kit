import '@testing-library/jest-dom/extend-expect';
import { render } from '../../utils/testUtils';
import { Stack } from './Stack';

describe('Stack', () => {
  it('should render children', () => {
    const { getByText, container } = render(
      <Stack spacing="medium">
        <div>First</div>
        <div>Second</div>
        <div>Last</div>
      </Stack>,
    );

    const stackElement = container.childNodes[0];

    expect(getByText('First')).toBeTruthy();
    expect(getByText('Last')).toBeTruthy();
    expect(stackElement.childNodes.length).toBe(3);
  });
});
