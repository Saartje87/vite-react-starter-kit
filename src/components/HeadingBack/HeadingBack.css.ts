import { globalStyle, style } from '@vanilla-extract/css';
import { theme } from '../../theme.css';

export const headingBackClass = style({
  cursor: 'pointer',
  paddingLeft: theme.space['small'],
  outline: 0,
});

globalStyle(`${headingBackClass}:is(:hover, :focus, :active) *`, {
  color: theme.color.blue,
});
