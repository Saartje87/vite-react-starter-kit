import { useHistory } from '@blockle/router';
import React, { FC } from 'react';
import { Heading } from '../Heading/Heading';
import { Icon } from '../Icon/Icon';
import { Stack } from '../Stack/Stack';
import { headingBackClass } from './HeadingBack.css';

export const HeadingBack: FC = ({ children }) => {
  const { back } = useHistory();

  function keyPress(event: React.KeyboardEvent<HTMLDivElement>) {
    if (event.key === ' ' || event.key === 'Enter') {
      back();
    }
  }

  return (
    <Stack
      spacing="medium"
      horizontal
      alignItems="center"
      role="button"
      tabIndex={0}
      onKeyPress={keyPress}
      onClick={back}
      className={headingBackClass}
    >
      <Icon name="back" size="small" color="darkBlue" />
      <Heading>{children}</Heading>
    </Stack>
  );
};
