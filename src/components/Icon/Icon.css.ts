import { globalStyle, style, styleVariants } from '@vanilla-extract/css';

export const iconClass = style({
  lineHeight: 0,
  overflow: 'hidden',
});

globalStyle(`${iconClass} svg`, {
  fill: 'currentcolor',
});

export const iconSizeVariants = styleVariants({
  small: {
    width: 12,
    height: 12,
  },
  medium: {
    width: 18,
    height: 18,
  },
});
