// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0';
import { Icon, Props as IconProps } from './Icon';

export default {
  title: 'Icon',
  component: Icon,
  argTypes: {
    className: {
      control: false,
    },
  },
} as Meta;

const Template: Story<IconProps> = (args) => <Icon {...args} />;

export const Default = Template.bind({});
Default.args = {
  color: 'darkBlue',
  name: 'email',
  size: 'medium',
};
