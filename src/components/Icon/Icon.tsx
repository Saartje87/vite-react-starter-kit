import { FC } from 'react';
import iconSpriteUrl from '../../assets/icons.svg';
import { theme } from '../../theme.css';
import { cx } from '../../utils/cx';
import { Box } from '../Box/Box';
import { iconClass, iconSizeVariants } from './Icon.css';

export type IconNames =
  | 'back'
  | 'check'
  | 'cross'
  | 'email'
  | 'interaction'
  | 'leaderboard'
  | 'logo-small'
  | 'menu'
  | 'settings'
  | 'shopping-cart'
  | 'user';

export interface Props {
  className?: string;
  size?: keyof typeof iconSizeVariants;
  name: IconNames;
  color?: keyof typeof theme.color;
}

export const Icon: FC<Props> = ({ className, size = 'medium', name, color = 'red' }) => {
  return (
    <Box
      display="flex"
      placeItems="center"
      color={color}
      className={cx(iconClass, iconSizeVariants[size], className)}
    >
      <svg>
        <use xlinkHref={`${iconSpriteUrl}#${name}`}></use>
      </svg>
    </Box>
  );
};
