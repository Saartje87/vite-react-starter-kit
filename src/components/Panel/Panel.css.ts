import { style } from '@vanilla-extract/css';
import { breakpoints, theme } from '../../theme.css';

export const panelClass = style({
  position: 'absolute',
  left: theme.space.gutter,
  right: theme.space.gutter,
  bottom: theme.space.gutter,
  top: 190,
  maxWidth: 440,
  borderRadius: 6,
  boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
  transition: 'opacity 220ms, transform 220ms cubic-bezier(0.33, 1, 0.68, 1)',
  contain: 'layout',
  overflow: 'hidden',
  selectors: {
    '&[aria-hidden="true"]': {
      pointerEvents: 'none',
      opacity: 0,
      transform: 'translateY(6%)',
    },
  },
  '@media': {
    [breakpoints.tablet]: {
      top: theme.space.gutter,
      right: 'auto',
      width: 375,
      selectors: {
        '&[aria-hidden="true"]': {
          transform: 'translateX(-100%)',
        },
      },
    },
  },
});
