import { forwardRef, ReactNode } from 'react';
import { Atoms } from '../../sprinkles.css';
import { Box } from '../Box/Box';
import { panelClass } from './Panel.css';

interface Props extends Atoms {
  open: boolean;
  children: ReactNode;
}

export const Panel = forwardRef<HTMLDivElement, Props>(function Panel(
  { children, open, ...atoms },
  ref,
) {
  return (
    <Box ref={ref} aria-hidden={!open} className={panelClass} background="white" {...atoms}>
      {children}
    </Box>
  );
});
