import { globalStyle } from '@vanilla-extract/css';
import { theme } from './theme.css';

globalStyle('html,body', {
  WebkitFontSmoothing: 'antialiased',
  WebkitTapHighlightColor: '#0000',
  textSizeAdjust: 'none',
  touchAction: 'manipulation',
  fontFamily: theme.fontFamily,
  fontWeight: theme.fontWeight.regular,
  fontSize: 16,
  margin: 0,
  padding: 0,
  // TODO REMOVE ME!
  background: 'linear-gradient(114deg, rgba(137,139,141,1) 0%, rgba(132,118,109,1) 100%)',
});

globalStyle('*', {
  boxSizing: 'border-box',
});

globalStyle('html', {
  lineHeight: 1.15,
  WebkitTextSizeAdjust: '100%',
});

globalStyle('html,body,#root', {
  height: '100%',
  width: '100%',
});

globalStyle('button,input,optgroup,select,textarea', {
  fontFamily: 'inherit',
  fontSize: '100%',
  lineHeight: 1.15,
  margin: 0,
  padding: 0,
});
