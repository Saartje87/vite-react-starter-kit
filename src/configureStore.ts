import { applyMiddleware, compose, createStore } from 'redux';
import { PersistConfig, persistReducer, persistStore } from 'redux-persist';
import { PersistPartial } from 'redux-persist/es/persistReducer';
import storage from 'redux-persist/es/storage';
import thunk from 'redux-thunk';
import { name } from '../package.json';
import { AppActions, AppState, rootReducer } from './store';

const isDevelopment = import.meta.env.MODE === 'development';

const persistConfig: PersistConfig<AppState> = {
  key: name,
  storage,
  whitelist: [],
};

const composeEnhancers = isDevelopment
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  : compose;

const persistedReducer = persistReducer(persistConfig, rootReducer);

const enhancer = composeEnhancers(applyMiddleware(thunk));

export function configureStore() {
  const store = createStore<AppState & PersistPartial, AppActions, Record<string, unknown>, void>(
    persistedReducer,
    enhancer,
  );

  const persistor = persistStore(store);

  // TODO Test HMR with reducers
  // if (import.meta.hot) {
  //   import.meta.hot?.accept('./store', (dep) => {
  //     const nextRootReducer = require('../reducers');
  //     store.replaceReducer(nextRootReducer);
  //   });
  // }

  return { store, persistor };
}
