import { compose } from 'redux';

interface EmConfig {
  backendHost: string;
  settings: Record<string, undefined | string | number>;
  version: string;
}

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    emconfig: EmConfig;
  }
}
