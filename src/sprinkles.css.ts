import { createAtomicStyles, createAtomsFn } from '@vanilla-extract/sprinkles';
import { breakpoints, theme } from './theme.css';

export const responsiveConfig = {
  conditions: {
    mobile: {},
    tablet: { '@media': breakpoints.tablet },
    desktop: { '@media': breakpoints.desktop },
  },
  defaultCondition: 'mobile',
  responsiveArray: ['mobile', 'tablet', 'desktop'],
} as const;

// Exported so storybook stories can access style options
export const responsiveStyles = createAtomicStyles({
  ...responsiveConfig,
  properties: {
    display: ['none', 'flex', 'block', 'inline', 'inline-block', 'grid', 'inline-grid'],
    flexDirection: ['row', 'column'],
    justifyContent: [
      'stretch',
      'flex-start',
      'center',
      'flex-end',
      'space-around',
      'space-between',
    ],
    alignItems: ['stretch', 'flex-start', 'center', 'flex-end'],
    paddingTop: theme.space,
    paddingBottom: theme.space,
    paddingLeft: theme.space,
    paddingRight: theme.space,
    flexGrow: [0, 1],
    fontSize: theme.fontSize,
  },
  shorthands: {
    padding: ['paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight'],
    paddingX: ['paddingLeft', 'paddingRight'],
    paddingY: ['paddingTop', 'paddingBottom'],
    placeItems: ['justifyContent', 'alignItems'],
  },
});

const staticStyles = createAtomicStyles({
  properties: {
    color: theme.color,
    background: theme.color,
    fontWeight: theme.fontWeight,
    textAlign: ['center', 'left', 'right', 'justify'],
    fontStyle: ['normal', 'italic', 'oblique'],
    textDecoration: ['overline', 'line-through', 'underline', 'none'],
    position: ['relative', 'fixed', 'absolute', 'sticky'],
    overflow: ['auto', 'hidden'],
  },
});

// Export all styles so we can use these in some storybook stories
export const atomicStyles = { ...responsiveStyles.styles, ...staticStyles.styles };

export const atoms = createAtomsFn(responsiveStyles, staticStyles);

export type Atoms = Parameters<typeof atoms>[0];
