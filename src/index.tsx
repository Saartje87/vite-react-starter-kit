import { render } from 'react-dom';
import { AppShell } from './components/AppShell/AppShell';
import { ServiceWorker } from './components/ServiceWorker/ServiceWorker';
import { configureStore } from './configureStore';
import './index.css';
import { RootScreen } from './screens/RootScreen/RootScreen';
import './theme.css';

const mountNode = document.getElementById('root');

if (!mountNode) {
  throw new Error("Mount node not found 'root'");
}

const { store, persistor } = configureStore();

render(
  <AppShell store={store} persistor={persistor}>
    <ServiceWorker />
    <RootScreen />
  </AppShell>,
  mountNode,
);

// Scroll to active input whenever viewport changes size
// This is most useful on mobile devices, input is not (always) visible after opening the virtual keyboard.
window.addEventListener('resize', () => {
  if (document.activeElement && document.activeElement.nodeName === 'INPUT') {
    document.activeElement.scrollIntoView({ block: 'center' });
  }
});
