import { Route, useHistory } from '@blockle/router';
import { FC } from 'react';
import { Box } from '../../components/Box/Box';
import { Button } from '../../components/Button/Button';
import { Heading } from '../../components/Heading/Heading';
import { ScreenTransition } from '../../components/ScreenTransition/ScreenTransition';
import { Stack } from '../../components/Stack/Stack';
import { useSetDrawerColor } from '../../hooks/useSetDrawerColor';
import { TextScreen } from '../TextScreen/TextScreen';
import { settingScreenButtonGroupClass } from './SettingsScreen.css';

export const SettingsScreen: FC = () => {
  const { push } = useHistory();

  useSetDrawerColor('yellow');

  return (
    <>
      <ScreenTransition level={0}>
        <Box padding="gutter" overflow="auto">
          <Stack spacing="medium">
            <Heading>Settings</Heading>

            <Box display="grid" className={settingScreenButtonGroupClass}>
              <Button
                onClick={() => {
                  push('/settings/text/terms-and-conditions');
                }}
              >
                Terms & Conditions
              </Button>
              <Button
                onClick={() => {
                  push('/settings/text/privacy-policy');
                }}
              >
                Privacy policy
              </Button>
            </Box>
          </Stack>
        </Box>
      </ScreenTransition>

      <Route
        path="/text/:id"
        render={(match, { id }) => {
          return match && <TextScreen id={id} />;
        }}
      />
    </>
  );
};
