import { style } from '@vanilla-extract/css';
import { theme } from '../../theme.css';

export const settingScreenButtonGroupClass = style({
  width: '100%',
  gridTemplateColumns: '1fr 1fr',
  gap: theme.space['large'],
});
