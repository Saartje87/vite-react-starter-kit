import { FC } from 'react';
import { Box } from '../../components/Box/Box';
import { HeadingBack } from '../../components/HeadingBack/HeadingBack';
import { ScreenTransition } from '../../components/ScreenTransition/ScreenTransition';
import { Stack } from '../../components/Stack/Stack';
import { Text } from '../../components/Text/Text';

interface Props {
  id: string;
}

export const TextScreen: FC<Props> = ({ id }) => {
  return (
    <ScreenTransition level={1}>
      <Box padding="gutter" overflow="auto">
        <Stack spacing="medium">
          <HeadingBack>{id}</HeadingBack>

          <Text as="p">
            What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting
            industry Lorem Ipsum has been the industrys standard dummy text ever since the 1500s
            when an unknown printer took a galley of type and scrambled it to make a type specimen
            book it has?
          </Text>
        </Stack>
      </Box>
    </ScreenTransition>
  );
};
