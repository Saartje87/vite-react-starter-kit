import { FC } from 'react';
import { Box } from '../../components/Box/Box';
import { Heading } from '../../components/Heading/Heading';
import { HeadingBack } from '../../components/HeadingBack/HeadingBack';
import { ScreenTransition } from '../../components/ScreenTransition/ScreenTransition';
import { Stack } from '../../components/Stack/Stack';
import { Text } from '../../components/Text/Text';

export const ProductDetailScreen: FC = () => {
  return (
    <ScreenTransition level={1}>
      <Box padding="gutter" overflow="auto">
        <Stack spacing="medium">
          <HeadingBack>Product details</HeadingBack>

          <Box>
            <img
              style={{ aspectRatio: '4/3', maxWidth: '100%' }}
              src="https://135dip1kp5pb1hxer93f2f2i-wpengine.netdna-ssl.com/wp-content/uploads/2017/12/jcpenney-ugly-sweater-for-two.jpg"
              alt="Product"
            />
          </Box>

          <Stack spacing="small" horizontal alignItems="center">
            <Text fontSize="large" fontWeight="bold" color="blue">
              $14,95
            </Text>
            <Text fontSize="medium" textDecoration="line-through">
              $39,95
            </Text>
          </Stack>

          <Heading as="h2" color="purple">
            Washable middle gauge knit crew neck jumper
          </Heading>

          <Stack spacing="small">
            <Text as="strong" fontSize="medium" fontWeight="bold">
              Material Composition:
            </Text>

            <Text as="p" fontSize="small">
              95% Acrylic, 5% Wool
            </Text>
          </Stack>

          <Stack spacing="small">
            <Text as="strong" fontSize="medium" fontWeight="bold">
              Care Instructions:
            </Text>

            <Text as="p" fontSize="small">
              Machine wash up to 40 degrees on a gentle cycle
            </Text>
          </Stack>

          <Stack spacing="small">
            <Text as="strong" fontSize="medium" fontWeight="bold">
              Size Information:
            </Text>

            <Text as="p" fontSize="small">
              This product is made according to men’s size guidelines. For women’s sizing, we
              recommend purchasing either 1 or 2 sizes smaller than your usual size.
            </Text>
          </Stack>

          <Stack spacing="small">
            <Text as="strong" fontSize="medium" fontWeight="bold">
              Description:
            </Text>

            <Text as="p" fontSize="small">
              A chunky knit that combines comfort with fashion. Convenient and versatile. Crew neck
              with a tighter collar line for a trendier look. Slightly loose-fitting design, so you
              can wear it on its own or over a shirt.
            </Text>
          </Stack>
        </Stack>
      </Box>
    </ScreenTransition>
  );
};
