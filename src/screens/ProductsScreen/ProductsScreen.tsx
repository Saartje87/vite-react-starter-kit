import { Route } from '@blockle/router';
import { FC } from 'react';
import { Box } from '../../components/Box/Box';
import { Heading } from '../../components/Heading/Heading';
import { ProductCard } from '../../components/ProductCard/ProductCard';
import { ScreenTransition } from '../../components/ScreenTransition/ScreenTransition';
import { Stack } from '../../components/Stack/Stack';
import { useSetDrawerColor } from '../../hooks/useSetDrawerColor';
import { ProductDetailScreen } from '../ProductDetailScreen/ProductDetailScreen';

export const ProductsScreen: FC = () => {
  useSetDrawerColor('purple');

  return (
    <>
      <ScreenTransition level={0}>
        <Box padding="gutter" overflow="auto">
          <Stack spacing="small">
            <Heading>Products</Heading>

            <Stack spacing="medium" justifyContent="stretch">
              <ProductCard />
              <ProductCard />
              <ProductCard />
              <ProductCard />
              <ProductCard />
              <ProductCard />
              <ProductCard />
              <ProductCard />
              <ProductCard />
              <ProductCard />
            </Stack>
          </Stack>
        </Box>
      </ScreenTransition>

      <Route path="/detail/:id">
        <ProductDetailScreen />
      </Route>
    </>
  );
};
