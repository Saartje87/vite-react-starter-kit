import { FC } from 'react';
import { Box } from '../../components/Box/Box';
import { Heading } from '../../components/Heading/Heading';
import { useSetDrawerColor } from '../../hooks/useSetDrawerColor';

export const InteractionsScreen: FC = () => {
  useSetDrawerColor('blue');

  return (
    <Box padding="gutter" overflow="auto">
      <Heading>Interactions</Heading>
    </Box>
  );
};
