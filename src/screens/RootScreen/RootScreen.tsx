import { Route, RouteGroup, useHistory } from '@blockle/router';
import { FC } from 'react';
import { Box } from '../../components/Box/Box';
import { Drawer } from '../../components/Drawer/Drawer';
import { ExtensionButton } from '../../components/ExtensionButton/ExtensionButton';
import { InteractionsScreen } from '../InteractionsScreen/InteractionsScreen';
import { ProductsScreen } from '../ProductsScreen/ProductsScreen';
import { SettingsScreen } from '../SettingsScreen/SettingsScreen';
import { rootScreenClass } from './RootScreen.css';

export const RootScreen: FC = () => {
  const { replace, location } = useHistory();

  return (
    <Box className={rootScreenClass} overflow="hidden">
      <ExtensionButton />

      <Drawer>
        <RouteGroup>
          <Route
            noMatch
            render={(match) => {
              if (match && location.pathname === '/') {
                replace('/interactions');
              }

              return null;
            }}
          />

          <Route path="/interactions">
            <InteractionsScreen />
          </Route>

          <Route path="/products">
            <ProductsScreen />
          </Route>

          <Route path="/settings">
            <SettingsScreen />
          </Route>
        </RouteGroup>
      </Drawer>
    </Box>
  );
};
