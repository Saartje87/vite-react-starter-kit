import { FC } from 'react';
import { Box } from '../../components/Box/Box';
import { Heading } from '../../components/Heading/Heading';
import { useSetDrawerColor } from '../../hooks/useSetDrawerColor/useSetDrawerColor';

export const NoMatchScreen: FC = () => {
  useSetDrawerColor('red');

  return (
    <Box padding="gutter">
      <Heading>No match</Heading>
    </Box>
  );
};
