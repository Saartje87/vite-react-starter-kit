import { useState } from 'react';
import { Box } from '../../components/Box/Box';
import { Button } from '../../components/Button/Button';
import { Dialog } from '../../components/Dialog/Dialog';
import { Heading } from '../../components/Heading/Heading';
import { Stack } from '../../components/Stack/Stack';
import { Text } from '../../components/Text/Text';
import { useSetDrawerColor } from '../../hooks/useSetDrawerColor/useSetDrawerColor';

export const StyleGuideScreen = () => {
  const [dialog, setDialog] = useState(false);

  useSetDrawerColor('red');

  return (
    <Box padding="gutter" overflow="auto">
      <Stack spacing="small">
        <Heading>Some header</Heading>

        <Stack spacing="large">
          <Stack spacing="small">
            <Text fontWeight="bold" fontSize="medium">
              Open dialog
            </Text>
            <Button onClick={() => setDialog(true)}>Dialog</Button>
          </Stack>

          <Stack spacing="small">
            <Text fontWeight="bold" fontSize="medium">
              Button
            </Text>
            <Button>Button Normal</Button>
            <Button disabled>Button Disabled</Button>
          </Stack>

          <Stack spacing="small">
            <Text fontWeight="bold" fontSize="medium">
              Outline Button
            </Text>
            <Button variant="outline">Outline Button Normal</Button>
            <Button variant="outline" disabled>
              Outline Button Disabled
            </Button>
          </Stack>
        </Stack>
      </Stack>

      <Dialog open={dialog} onRequestClose={() => setDialog(false)}>
        <Stack spacing="large">
          <Stack spacing="small">
            <Text as="h2" fontWeight="bold" fontSize="large">
              Wockeltjes
            </Text>
            <Text as="p" fontSize="small">
              Nec quam tellus urna laoreet at netus varius. Commodo semper mattis tortor diam sed.
            </Text>
          </Stack>

          <Button onClick={() => setDialog(false)}>Close</Button>
        </Stack>
      </Dialog>
    </Box>
  );
};
