import { assignVars, createGlobalTheme, globalStyle } from '@vanilla-extract/css';

export const breakpoints = {
  mobile: '',
  tablet: 'screen and (min-width: 768px)',
  desktop: 'screen and (min-width: 1024px)',
};

export const theme = createGlobalTheme(':root', {
  space: {
    gutter: '10px',
    small: '5px',
    medium: '10px',
    large: '15px',
    xlarge: '20px',
  },
  color: {
    white: '#fff',
    grey: '#EDF2F6',
    darkBlue: '#404B69',
    blueDisabled: '#BFD7E8',
    blue: '#39AFFD',
    blueHover: '#339EE4',
    green: '#02C39A',
    red: '#F73859',
    purple: '#8F71FF',
    yellow: '#FFBC42',
  },
  fontFamily: "'Roboto', sans-serif",
  fontSize: {
    small: '14px',
    medium: '16px',
    large: '18px',
  },
  fontWeight: {
    regular: '400',
    bold: '500',
  },
});

globalStyle(':root', {
  '@media': {
    [breakpoints.tablet]: {
      vars: assignVars(theme.fontSize, {
        small: '12px',
        medium: '14px',
        large: '16px',
      }),
    },
  },
});
