import { combineReducers } from 'redux';
import { DrawerActions } from './drawer/drawerActions';
import { drawerReducer } from './drawer/drawerReducer';

export const rootReducer = combineReducers({
  drawer: drawerReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
export type AppActions = DrawerActions;
