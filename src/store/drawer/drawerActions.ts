import { theme } from '../../theme.css';
import { createAction, createActionWithPayload } from '../../utils/redux';

export const openDrawer = () => createAction('@drawer/OPEN');

export const closeDrawer = () => createAction('@drawer/CLOSE');

export const setDrawerColor = (color: keyof typeof theme.color) =>
  createActionWithPayload('@drawer/SET_COLOR', color);

export type DrawerActions = ReturnType<
  typeof openDrawer | typeof closeDrawer | typeof setDrawerColor
>;
