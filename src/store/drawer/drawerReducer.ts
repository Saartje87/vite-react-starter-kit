import { DrawerActions } from './drawerActions';
import { DrawerState } from './types';

const initialState: DrawerState = {
  open: false,
  color: 'blue',
};

export const drawerReducer = (state = initialState, action: DrawerActions): DrawerState => {
  switch (action.type) {
    case '@drawer/OPEN':
      return {
        ...state,
        open: true,
      };

    case '@drawer/CLOSE':
      return {
        ...state,
        open: false,
      };

    case '@drawer/SET_COLOR':
      return {
        ...state,
        color: action.payload,
      };

    default:
      return state;
  }
};
