import { theme } from '../../theme.css';

export interface DrawerState {
  open: boolean;
  color: keyof typeof theme.color;
}
