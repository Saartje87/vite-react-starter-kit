import { AppState } from '..';

export const isDrawerOpen = (state: AppState) => state.drawer.open;

export const getDrawerColor = (state: AppState) => state.drawer.color;
