import { useEffect, useRef } from 'react';
import { FocusTrap } from '../../utils/focusTrap';

export const useFocusTrap = () => {
  const ref = useRef<{ focusTrap: FocusTrap | null }>({ focusTrap: null });

  if (!ref.current.focusTrap) {
    ref.current.focusTrap = new FocusTrap();
  }

  useEffect(() => () => ref.current.focusTrap?.destroy(), []);

  return ref.current.focusTrap;
};
