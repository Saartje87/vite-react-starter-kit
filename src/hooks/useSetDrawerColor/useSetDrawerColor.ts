import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setDrawerColor } from '../../store/drawer/drawerActions';
import { theme } from '../../theme.css';

export const useSetDrawerColor = (color: keyof typeof theme.color) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setDrawerColor(color));
  }, []);
};
