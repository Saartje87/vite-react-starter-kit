import { useEffect } from 'react';
import { breakpoints } from '../../theme.css';

export const useBreakpoint = (
  breakpoint: keyof typeof breakpoints,
  onChange: (matches: boolean) => void,
) => {
  useEffect(() => {
    const media = matchMedia(breakpoints[breakpoint]);

    function callback() {
      onChange(media.matches);
    }

    media.addEventListener('change', callback);

    return () => {
      media.removeEventListener('change', callback);
    };
  }, [breakpoint]);
};
