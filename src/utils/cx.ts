type Args = null | undefined | boolean | string;

export const cx = (...args: Args[]): string =>
  args.filter((arg) => arg && typeof arg === 'string').join(' ');
