import { createAction, createActionWithPayload } from './action';

describe('Redux action utils', () => {
  it('should create action with type', () => {
    const action = createAction('ACTION_TYPE');

    expect(action).toEqual({ type: 'ACTION_TYPE' });
  });

  it('should create action with type and payload', () => {
    const action = createActionWithPayload('ACTION_TYPE', { pay: 'load' });

    expect(action).toEqual({ type: 'ACTION_TYPE', payload: { pay: 'load' } });
  });

  it('should create action with type, payload and meta', () => {
    const action = createActionWithPayload('ACTION_TYPE', { pay: 'load' }, { me: 'ta' });

    expect(action).toEqual({ type: 'ACTION_TYPE', payload: { pay: 'load' }, meta: { me: 'ta' } });
  });
});
