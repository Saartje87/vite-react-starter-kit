const focusSelector =
  'a[href]:not([disabled]),button:not([disabled]),textarea:not([disabled]),' +
  'input[type="text"]:not([disabled]),input[type="radio"]:not([disabled]),' +
  'input[type="checkbox"]:not([disabled]),select:not([disabled]),' +
  '[tabindex]:not([tabindex="-1"])';

export class FocusTrap {
  private element: HTMLElement | null = null;
  private active = false;
  private previousActiveElement: HTMLElement | null = null;

  destroy() {
    document.removeEventListener('focusin', this.focusTrap);

    this.element = null;
    this.previousActiveElement = null;
  }

  enable(element: HTMLElement) {
    if (!element) {
      throw new Error('Element does not exist');
    }

    this.active = true;
    this.element = element;
    this.previousActiveElement = document.activeElement as HTMLElement;

    if (this.previousActiveElement) {
      this.previousActiveElement.blur();
    }

    document.addEventListener('focusin', this.focusTrap);
  }

  disable() {
    if (this.previousActiveElement) {
      this.previousActiveElement.focus();
    }

    this.active = false;
    this.element = null;
    this.previousActiveElement = null;

    document.removeEventListener('focusin', this.focusTrap);
  }

  focusTrap = (event: FocusEvent) => {
    if (!this.element || !this.active || !event.target) {
      return;
    }

    // Check if focus is inside element
    if (!this.element.contains(event.target as HTMLElement)) {
      const focusTarget = this.getFirstFocusableTargetOf(this.element);

      event.preventDefault();

      if (focusTarget) {
        focusTarget.focus();
      }
    }
  };

  getFirstFocusableTargetOf(element: HTMLElement) {
    return element.querySelector<HTMLElement>(focusSelector);
  }
}
