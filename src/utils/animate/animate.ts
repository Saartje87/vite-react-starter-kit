interface Animate {
  duration: number;
  onCancel?: () => void;
  onEnd?: () => void;
  onTick: (progress: number) => void;
  timingFunction?: (progress: number) => number;
}

/**
 * const stop = animate({
 *  duration: 1000,
 *  timingFunction: easeOutCubic,
 *  onTick: (progress) => { console.log(progress) },
 *  onEnd: () => console.log('Animation ended')
 *  onCancel: () => console.log('Animation cancelled')
 * });
 *
 * stop(); // Stop / cancel animation
 */
export function animate({ duration, onCancel, onEnd, onTick, timingFunction = linear }: Animate) {
  const start = Date.now();
  let rAF: number;

  function frame() {
    const now = Date.now();
    const progress = Math.min(1, (now - start) / duration);

    onTick(timingFunction(progress));

    if (progress >= 1) {
      if (onEnd) {
        onEnd();
      }

      return;
    }

    rAF = requestAnimationFrame(frame);
  }

  rAF = requestAnimationFrame(frame);

  return () => {
    if (onCancel) {
      onCancel();
    }

    cancelAnimationFrame(rAF);
  };
}

// https://easings.net/
export function linear(x: number): number {
  return x;
}

export function easeOutCubic(x: number): number {
  return 1 - Math.pow(1 - x, 3);
}
