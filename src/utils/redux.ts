import { AnyAction } from 'redux';
import { ThunkAction as ReduxThunkAction } from 'redux-thunk';
import { AppActions, AppState } from '../store';

export interface Action<Type extends string> extends AnyAction {
  type: Type;
}

export interface ActionWithPayload<Type extends string, Payload, Meta> extends Action<Type> {
  type: Type;
  payload: Payload;
  meta?: Meta;
}

export const createAction = <Type extends string>(type: Type): Action<Type> => ({
  type,
});

export const createActionWithPayload = <Type extends string, Payload, Meta = unknown>(
  type: Type,
  payload: Payload,
  meta?: Meta,
): ActionWithPayload<Type, Payload, Meta> => ({
  type,
  payload,
  meta,
});

export type ThunkAction<R = void> = ReduxThunkAction<R, AppState, void, AppActions>;
