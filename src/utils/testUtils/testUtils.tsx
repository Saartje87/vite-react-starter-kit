import { Router } from '@blockle/router';
import '@testing-library/jest-dom/extend-expect';
import { render as testRender } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { ScreenTransitionProvider } from '../../components/ScreenTransition/ScreenTransitionProvider';

export * from '@testing-library/react';

const mockStore = configureStore([thunk]);

export const render = (Component: React.ReactElement, initialState = {}) => {
  const store = mockStore(initialState);

  const appShell = (Component: React.ReactElement) => (
    <Provider key="store" store={store}>
      <ScreenTransitionProvider>
        <Router history={createMemoryHistory()}>{Component}</Router>
      </ScreenTransitionProvider>
    </Provider>
  );

  const renderUtils = testRender(appShell(Component));
  const { container, baseElement } = renderUtils;

  return {
    store,
    ...renderUtils,
    rerender: (Component: React.ReactElement) => {
      testRender(appShell(Component), { container, baseElement });
    },
  };
};
