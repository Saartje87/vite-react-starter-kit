import { atomicStyles } from '../../sprinkles.css';

interface mapAtomicStylesToArgTypesProp {
  type: 'select';
  options: string[];
}

/**
 * Map atomic styles to argTypes for stories
 */
export function mapAtomicStylesToArgTypes(
  styles: typeof atomicStyles,
): Record<string, mapAtomicStylesToArgTypesProp> {
  const map: Record<string, mapAtomicStylesToArgTypesProp> = {};
  for (const [key, value] of Object.entries(styles)) {
    if ('values' in value) {
      map[key] = {
        type: 'select',
        options: Object.keys(value.values),
      };
    }
  }

  for (const [key, value] of Object.entries(styles)) {
    if ('mappings' in value) {
      map[key] = map[value.mappings[0]];
    }
  }

  return map;
}
