# vite-react-starter-kit

## Commands

### Start dev server

`yarn start`

### Build

`yarn build`

### Update sprite map

To add / remove icons, place them `<root>/icons`. Don't forget to add typings to `Icon.tsx`

`yarn update-sprite`

## Help

NodeJS warning / errors? Run `nvm use` in root directory.
